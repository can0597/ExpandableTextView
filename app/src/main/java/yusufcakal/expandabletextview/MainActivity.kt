package yusufcakal.expandabletextview

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.view.View
import at.blogc.android.views.ExpandableTextView
import android.view.animation.OvershootInterpolator
import android.widget.Button


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val expandableTextView = this.findViewById<View>(R.id.expandableTextView) as ExpandableTextView
        val buttonToggle = this.findViewById<View>(R.id.button_toggle) as Button

        expandableTextView.setAnimationDuration(750L)

        expandableTextView.setInterpolator(OvershootInterpolator())

        expandableTextView.expandInterpolator = OvershootInterpolator()
        expandableTextView.collapseInterpolator = OvershootInterpolator()

        buttonToggle.setOnClickListener {
            buttonToggle.setText(if (expandableTextView.isExpanded) R.string.expand else R.string.collapse)
            expandableTextView.toggle()
        }

        buttonToggle.setOnClickListener {
            if (expandableTextView.isExpanded) {
                expandableTextView.collapse()
                buttonToggle.setText(R.string.expand)
            } else {
                expandableTextView.expand()
                buttonToggle.setText(R.string.collapse)
            }
        }

    }
}
